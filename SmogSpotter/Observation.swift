//
//  Observation.swift
//  SmogSpotter
//
//  Created by mallocs.net on 9/22/20.
//

import Foundation
import MapKit

struct Observation: Identifiable {
    let id = UUID()
    var timestamp: Date = Date()
    var placemark: CLPlacemark? = nil
    var coordinate: CLLocationCoordinate2D? = nil
    var licensePlateNumber: String? = nil
    var vehicleMake: VehicleMake? = nil
    var vehicleType: VehicleType? = nil
    var street: String? = nil
    var city: String? = nil
    var county: County? = nil
    // var image: UIImage? = nil
}


enum VehicleType: String, CaseIterable, Identifiable {
    var id: String { self.rawValue }
    case car = "Car"
    case compact = "Compact"
    case crossover = "Crossover"
    case minivan = "Minivan"
    case jeep = "Jeep"
    case pickup = "Pickup"
    case sedan = "Sedan"
    case sportsCar = "Sports Car"
    case stationWagon = "Station Wagon"
    case suv = "SUV"
    case truck = "Truck"
    case van = "Van"
    static var regex: String {
        get {
            return #"\b[Tt]ype.?.?("# + VehicleType.allCases.map { $0.rawValue.lowercased() }.joined(separator: "|") + #")\b"#
        }
    }
    static func fromLooseRawValue(_ rawValue: String) -> Self? {
        if rawValue == "suv" || rawValue == "Suv" || rawValue == "SUV" {
            return .suv
        }
        return Self(rawValue: rawValue.capitalized)
    }
}

enum VehicleMake: String, CaseIterable, Identifiable {
    var id: String { self.rawValue }
    case acura = "Acura"
    case audi = "Audi"
    case bmw = "BMW"
    case buick = "Buick"
    case cadillac = "Cadillac"
    case chevrolet = "Chevrolet"
    case chrysler = "Chrysler"
    case dodge = "Dodge"
    case fiat = "Fiat"
    case ford = "Ford"
    case gmc = "GMC"
    case honda = "Honda"
    case hyundai = "Hyundai"
    case infiniti = "Infiniti"
    case jaguar = "Jaguar"
    case jeep = "Jeep"
    case kia = "Kia"
    case landRover = "Land Rover"
    case lexus = "Lexus"
    case lincoln = "Lincoln"
    case mazda = "Mazda"
    case mercedesBenz = "Mercedes Benz"
    case mini = "Mini"
    case mitsubishi = "Mitsubishi"
    case nissan = "Nissan"
    case porsche = "Porsche"
    case ramTrucks = "Ram Trucks"
    case suburu = "Suburu"
    case toyota = "Toyota"
    case volkswagen = "Volkswagen"
    case volvo = "Volvo"
    static var regex: String {
        get {
            let specialCasesString = specialCases.map { $0.value.map { $0 }.joined(separator: "|")  }.joined(separator: "|")
            let normalCasesString = Self.allCases.map { $0.rawValue.lowercased() }.joined(separator: "|")
            return #"\b[Mm]ake.?.?("# + normalCasesString + "|" + specialCasesString + #")\b"#
        }
    }
    // These are mostly for use with the regex when multiple phrases can map to the same case
    static var specialCases: [Self:[String]] {
        get {
            return [
                .mercedesBenz: ["mercedes"],
                .ramTrucks: ["ram", "ram truck"]
            ]
        }
    }
    // These are for when the case's rawValue is capitalized unusually or when multiple phrases should map to the same case
    static func fromLooseRawValue(_ rawValue: String) -> Self? {
        if rawValue == "bmw" {
            return .bmw
        }
        if rawValue == "gmc" {
            return .gmc
        }
        if rawValue == "mercedes" {
            return .mercedesBenz
        }
        if rawValue == "ram" || rawValue == "ram truck" {
            return .ramTrucks
        }
        return Self(rawValue: rawValue.capitalized)
    }
}

enum County: String, CaseIterable, Identifiable {
    var id: String { self.rawValue }

    case carsonCity = "carson city"
    case churchill, clark, douglas, elko, esmeralda, eureka, humboldt, lander, lincoln, lyon, mineral, nye, pershing, storey, washoe
    case whitePine = "white pine"
    var print: String {
        self.rawValue.capitalized
    }
}
