//
//  Array+Identifiable.swift
//  Memorize
//
//  Created by mallocs.net on 7/18/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import Foundation


extension Array where Element: Identifiable {
    func firstIndex(matching: Element) -> Int? {
        for index in 0..<self.count {
            if self[index].id == matching.id {
                return index
            }
        }
        return nil
    }

    func firstIndex(matchingId: UUID) -> Int? {
        for index in 0..<self.count {
            if self[index].id as! UUID == matchingId {
                return index
            }
        }
        return nil
    }
}

