//
//  AudioRecorder.swift
//  SmogSpotter
//
//  Created by mallocs.net on 10/11/20.
//  From: https://gist.github.com/sssbohdan/0cad6dacdb692e7890f1d9048008eef1
//

import SwiftUI
import AVFoundation
import Speech

typealias SpeechCompletionHandler = () -> Void

protocol AudioRecorder {
    func checkPermission(_ completion: ((Bool) -> Void)?)

    // if url is nil audio will be stored to default url
    func record(to url: URL?)
    func stopRecording()

    // if url is nil audio will be played from default url
    func play(from url: URL?)
    func stopPlaying()

    func pause()
    func resume()
}

final class AudioRecorderImpl: NSObject, AudioRecorder, ObservableObject {
    private let session = AVAudioSession.sharedInstance()
    private let audioEngine = AVAudioEngine()
    private var player: AVAudioPlayer?
    private var recorder: AVAudioRecorder?
    private let synth = AVSpeechSynthesizer()
    private let voice = AVSpeechSynthesisVoice(language: "en-US")
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    private var recognitionTask: SFSpeechRecognitionTask?
    private (set) var permissionGranted = false
    @Published private (set) var isRecording = false
    @Published private (set) var isPlaying = false
    @Published private (set) var isSpeaking = false
    @Published private (set) var transcript: String?
    @Published private (set) var hasRecordingPublished = false

    private (set) var fileURL: URL?
    private let settings = [
        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
        AVSampleRateKey: 44100,
        AVNumberOfChannelsKey: 2,
        AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
    ]
    var handleSpeechCompletion: SpeechCompletionHandler?
  
    override init() {
        super.init()
        synth.delegate = self
        if speechRecognizer != nil {
            speechRecognizer!.delegate = self
        }
        fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("note.m4a")
    }

    func fileCreationDate(/* url: URL */) -> Date? {
        guard let url = fileURL else {
            return nil
        }
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: url.path)
            return attr[FileAttributeKey.creationDate] as? Date
        } catch {
            return nil
        }
    }
    
    func deleteFile(/* url: URL */) {
        guard let url = fileURL else {
            return
        }
        do {
            hasRecordingPublished = false
            try FileManager.default.removeItem(at: url)
        } catch let error as NSError {
            print("Error: \(error.domain)")
        }
    }
    
    var hasRecording: Bool {
        get {
            hasRecordingPublished = fileURL != nil && FileManager.default.fileExists(atPath: fileURL!.path)
            return hasRecordingPublished
        }
    }
    
    func speak(_ utterance: String, handleSpeechCompletion: SpeechCompletionHandler? = nil) {
        self.handleSpeechCompletion = handleSpeechCompletion
        // Let the user know to start talking.
        let utterance = AVSpeechUtterance(string: utterance)
        utterance.voice = voice
        utterance.rate = 0.52
        try? session.setCategory(.playback, mode: .spokenAudio, options: [.duckOthers, .defaultToSpeaker])
        try? session.setActive(true)
        synth.speak(utterance)
        try? session.setActive(false)
    }

    private func prepareRecognitionRequest() {
        if audioEngine.isRunning {
            audioEngine.stop()
      //      recognitionRequest?.endAudio()
        }
        // Cancel the previous task if it's running.
        recognitionTask?.cancel()
        self.recognitionTask = nil
    }

    func startRecordingWithTranscription() throws {
        guard let speechRecognizer = self.speechRecognizer else {
           // A recognizer is not supported for the current locale
           return
        }
        prepareRecognitionRequest()

        // Create and configure the speech recognition request.
        let recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
//        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to create a SFSpeechAudioBufferRecognitionRequest object") }
        recognitionRequest.shouldReportPartialResults = false
        
        // Keep speech recognition data on device
        if #available(iOS 13, *) {
            recognitionRequest.requiresOnDeviceRecognition = true
        }
        
        // Configure the microphone input.
        let inputNode = audioEngine.inputNode
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: inputNode.outputFormat(forBus: 0)) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            recognitionRequest.append(buffer)
        }
        
        audioEngine.prepare()
        try audioEngine.start()
        
        // Create a recognition task for the speech recognition session.
        // Keep a reference to the task so that it can be canceled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false

            if let result = result {
                // Update the text view with the results.
                self.transcript = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }

            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionTask = nil
            }
        }
    }

    func extractLicensePlateObservation(_ result: SFSpeechRecognitionResult) -> String? {
        let licensePlate = result.bestTranscription.formattedString.groups(for:#"\b[Pp]late.?.?\b([A-Z0-9 ]{2,7})\b"#) // TODO: should accept license plate number ###
        let result = licensePlate[0, default: []][1, default: ""]
        return result == "" ? nil : result
    }
    func extractVehicleMakeObservation(_ result: SFSpeechRecognitionResult) -> VehicleMake? {
        let vehicleMake = result.bestTranscription.formattedString.lowercased().groups(for: VehicleMake.regex)
        return VehicleMake.fromLooseRawValue(vehicleMake[0, default: []][1, default: ""])
    }
    func extractVehicleTypeObservation(_ result: SFSpeechRecognitionResult) -> VehicleType? {
        let vehicleType = result.bestTranscription.formattedString.lowercased().groups(for: VehicleType.regex)
        return VehicleType.fromLooseRawValue(vehicleType[0, default: []][1, default: ""])
    }

    func checkPermissionsAndTranscribe(
        _ url: URL?,
        handleTranscriptionCompletion: ((SFSpeechRecognitionResult?, Error?) -> Void)? = nil
    ) {
        checkSpeechRecognitionPermission() { isAuthorized in
            if isAuthorized && self.hasRecording {
                self.transcribe(url, handleTranscriptionCompletion: handleTranscriptionCompletion)
            }
        }
    }

    func transcribe(
        _ url: URL?,
        handleTranscriptionCompletion: ((SFSpeechRecognitionResult?, Error?) -> Void)? = nil
    ) {

        guard permissionGranted,
              let url = url ?? fileURL else { return }
        guard let speechRecognizer = self.speechRecognizer else {
           // A recognizer is not supported for the current locale
           return
        }
        
//        if !speechRecognizer.isAvailable {
//           // The recognizer is not available right now
//           return
//        }

        prepareRecognitionRequest()

        // Create and configure the speech recognition request.
        let recognitionRequest = SFSpeechURLRecognitionRequest(url: url)
        recognitionRequest.taskHint = .dictation
        recognitionRequest.shouldReportPartialResults = false
        
        // Keep speech recognition data on device
        if #available(iOS 13, *) {
            recognitionRequest.requiresOnDeviceRecognition = false
        }

        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { (result, error) in
            guard let result = result else {
                debugPrint(error)
                // Recognition failed, so check error for details and handle it
                return
            }

            // Print the speech that has been recognized so far
            if result.isFinal {
                if handleTranscriptionCompletion != nil {
                    handleTranscriptionCompletion!(result, error)
                }
                self.transcript = result.bestTranscription.formattedString
                print("Speech in the file is \(result.bestTranscription.formattedString)")
            }
            self.audioEngine.stop()
            self.recognitionTask = nil
        }
    }
  
    func record(to url: URL?) {
        audioEngine.inputNode.removeTap(onBus: 0)
        guard permissionGranted,
        let url = url ?? fileURL else { return }

        setupRecorder(url: url)

        if isRecording {
            stopRecording()
        }

        isRecording = true
        recorder?.record()
    }
  
    func stopRecording() {
        audioEngine.stop()
        isRecording = false
        recorder?.stop()
        try? session.setActive(false)
    }

    func stopRecording(handleCompletion: (() -> Void)? = nil) {
        synth.stopSpeaking(at: AVSpeechBoundary.immediate)
        self.stopRecording()
        if handleCompletion != nil {
            handleCompletion!()
        }
    }
  
    func play(from url: URL?) {
        guard let url = url ?? fileURL else { return }

        setupPlayer(url: url)

        if isRecording {
            stopRecording()
        }

        if isPlaying {
            stopPlaying()
        }

        if FileManager.default.fileExists(atPath: url.path) {
            isPlaying = true
            setupPlayer(url: url)
            player?.play()
        }
    }
  
    func stopPlaying() {
        isPlaying = false
        player?.stop()
    }
  
    func pause() {
        player?.pause()
    }

    func resume() {
        if player?.isPlaying == false {
            player?.play()
        }
    }
    
    func checkSpeechRecognitionPermission(_ completion: ((Bool) -> Void)? = nil) {
        switch SFSpeechRecognizer.authorizationStatus() {
        case .notDetermined:
            SFSpeechRecognizer.requestAuthorization { authStatus in
                DispatchQueue.main.async {
                    switch authStatus {
                    case .authorized: if completion != nil {
                        completion!(true)
                    }
                    default: self.handleSpeechRecognitionPermissionFailed()
                    }
                }
            }
        case .authorized: if completion != nil {
            completion!(true)
        }
        default: break // TODO: ask to change settings?
        }

    }
    private func handleSpeechRecognitionPermissionFailed() {
        // Present an alert asking the user to change their settings.
//        let ac = UIAlertController(title: "This app must have access to speech recognition to work.",
//                                   message: "Please consider updating your settings.",
//                                   preferredStyle: .alert)
//        ac.addAction(UIAlertAction(title: "Open settings", style: .default) { _ in
//            let url = URL(string: UIApplication.openSettingsURLString)!
//            UIApplication.shared.open(url)
//        })
    }
  
    func checkPermission(_ completion: ((Bool) -> Void)? = nil) {
        
        func assignAndInvokeCallback(_ granted: Bool) {
            self.permissionGranted = granted
            completion?(granted)
        }

        switch session.recordPermission {
            case .granted:
                assignAndInvokeCallback(true)

            case .denied:
                assignAndInvokeCallback(false)

            case .undetermined:
                session.requestRecordPermission(assignAndInvokeCallback)
        }
    }
}

extension AudioRecorderImpl: AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer,
                                     successfully flag: Bool) {
        isPlaying = false
    }
}

extension AudioRecorderImpl: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        isSpeaking = true
    }

    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        if let handleSpeechCompletion = self.handleSpeechCompletion {
            handleSpeechCompletion()
        }
        handleSpeechCompletion = nil
        isSpeaking = false
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
        isSpeaking = false
    }
}

extension AudioRecorderImpl: SFSpeechRecognizerDelegate {
    
}

private extension AudioRecorderImpl {
    func setupRecorder(url: URL) {
        guard permissionGranted else { return }

        try? session.setCategory(.playAndRecord, mode: .spokenAudio, options: [.duckOthers, .defaultToSpeaker])
        try? session.setActive(true)
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        do {
            recorder = try AVAudioRecorder(url: url, settings: settings)
        } catch {
            self.recorder = nil
            print(error.localizedDescription)
            print("AVAudioRecorder init failed")
        }
        recorder?.delegate = self
        recorder?.isMeteringEnabled = true
        recorder?.prepareToRecord()
    }
  
    func setupPlayer(url: URL) {
        do {
            player = try AVAudioPlayer(contentsOf: url)
        } catch {
            self.player = nil
            print(error.localizedDescription)
            print("AVAudioPlayer init failed")
        }
        player?.delegate = self
        player?.prepareToPlay()
    }
}

extension String {
  func separate(every: Int, with separator: String) -> String {
    return String(stride(from: 0, to: Array(self).count, by: every).map {
       Array(Array(self)[$0..<min($0 + every, Array(self).count)])
       }.joined(separator: separator))
  }
}


extension String {
    func groups(for regexPattern: String) -> [[String]] {
        do {
            let text = self
            let regex = try NSRegularExpression(pattern: regexPattern)
            let matches = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return matches.map { match in
                return (0..<match.numberOfRanges).map {
                    let rangeBounds = match.range(at: $0)
                    guard let range = Range(rangeBounds, in: text) else {
                        return ""
                    }
                    return String(text[range])
                }
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}

extension Array {
    public subscript(index: Int, default defaultValue: @autoclosure () -> Element) -> Element {
        guard index >= 0, index < endIndex else {
            return defaultValue()
        }

        return self[index]
    }
}
