//
//  AudioRecorderButton.swift
//  SmogSpotter
//
//  Created by mallocs.net on 9/23/20.
//

import SwiftUI

typealias SubmitCompletionHandler = (String?) -> Void

struct SubmitButton: View {
    @EnvironmentObject var store: ObservationStore
    var disable: Bool
    let submitCompletionHandler: SubmitCompletionHandler?

    var body: some View {
        Button(action: {
            store.mostRecentObservation.submit(submitCompletionHandler)
        }) {
            HStack {
                Text("Submit")
                Image(systemName: "paperplane.circle")
                    .imageScale(.large)
                    .foregroundColor(foregroundColor)
            }
        }.padding(.vertical, 13)
        .frame(maxWidth: .infinity)
        .foregroundColor(foregroundColor)
        .background(backgroundColor)
        .font(.title3)
        .border(backgroundColor)
        .cornerRadius(12)
        .disabled(disable)
        .opacity(disable ? 0.5 : 1)
    }

    let fileURL: URL? = nil
    let foregroundColor = Color.white
    let backgroundColor = Color.blue
}

struct ClearButton: View {
    @EnvironmentObject var store: ObservationStore
    @ObservedObject var audioRecorder: AudioRecorderImpl

    var body: some View {
        Button(action: {
            audioRecorder.deleteFile()
            store.mostRecentObservation.reset()
        }) {
            HStack {
                Text("Clear")
                Image(systemName: "trash")
                    .imageScale(.large)
                    .foregroundColor(foregroundColor)
            }
        }
        .padding(.vertical, 13)
        .frame(maxWidth: .infinity)
        .foregroundColor(foregroundColor)
        .background(backgroundColor)
        .font(.title3)
        .border(backgroundColor)
        .cornerRadius(12)
        .disabled(disable)
        .opacity(disable ? 0.5 : 1)
    }

    var disable: Bool {
        get {
            return audioRecorder.isRecording || audioRecorder.isSpeaking
        }
    }
    let fileURL: URL? = nil
    let foregroundColor = Color.white
    let backgroundColor = Color(UIColor.systemGray)
}


struct PlayButton: View {
    @ObservedObject var audioRecorder: AudioRecorderImpl

    var body: some View {
        Button(action: {
            if audioRecorder.isPlaying {
                audioRecorder.stopPlaying()
            } else {
                audioRecorder.play(from: fileURL)
            }
        }) {
            HStack {
                Text("Play")
                Image(systemName: audioRecorder.isPlaying ? "stop" : "play.rectangle")
                    .imageScale(.large)
                    .foregroundColor(foregroundColor)
            }
        }
        .padding(.vertical, 15)
        .frame(maxWidth: .infinity)
        .foregroundColor(foregroundColor)
        .background(backgroundColor)
        .font(.title3)
        .border(backgroundColor)
        .cornerRadius(12)
        .disabled(disable)
        .opacity(disable ? 0.5 : 1)
    }

    var disable: Bool {
        get {
            return audioRecorder.isRecording || audioRecorder.isSpeaking
        }
    }
    let fileURL: URL? = nil
    let foregroundColor = Color.white
    let backgroundColor = Color(UIColor.systemGray)
}


struct RecordButton: View {
    @EnvironmentObject var store: ObservationStore
    @ObservedObject var audioRecorder: AudioRecorderImpl
    @State var isRecording: Bool = false

    var body: some View {
        Button(action: {
            if self.isRecording {
                self.isRecording = false
                self.audioRecorder.stopRecording() {
                    if !store.mostRecentObservation.isInitialized {
                        store.mostRecentObservation.initObservation()
                    }
                    self.updateObservationFromRecording(fileURL)
                }
            } else {
                self.isRecording = true
                DispatchQueue.main.async {
                    self.audioRecorder.speak(recordMessage) {
                        self.audioRecorder.record(to: fileURL)
                    }
                }
            }
        }) {
            HStack {
                Text(store.mostRecentObservation.isInitialized ? "Redo Observation" : "Record Observation")
                    .padding(.trailing, 10)
                    .multilineTextAlignment(.center)
                Image(systemName: isRecording ? "stop" : "waveform.circle")
                    .imageScale(.large)
                    .foregroundColor(foregroundColor)
            } // arrow.clockwise
        }.onAppear {
            self.audioRecorder.checkPermission { (accessGranted) in
                if !accessGranted {
                    print("but why?")
                }
            }
//            if self.audioRecorder.hasRecordingComputed {
//                self.updateObservationFromRecording(fileURL, timestamp: self.audioRecorder.fileCreationDate())
//            }
        }
        .frame(maxWidth: .infinity)
        .padding(.vertical, 45)
        .foregroundColor(foregroundColor)
        .background(store.mostRecentObservation.isInitialized ? rerecordColor : backgroundColor)
        .cornerRadius(12)
        .font(.system(size: 45.0))
    }

    func updateObservationFromRecording(_ url: URL?, timestamp: Date? = nil) {
        self.audioRecorder.checkPermissionsAndTranscribe(fileURL) { results, error in
            if let results = results {
                    self.store.updateObservation(
                        store.mostRecentObservation.model.id,
                        timestamp: timestamp,
                        licensePlateNumber: self.audioRecorder.extractLicensePlateObservation(results),
                        vehicleMake: self.audioRecorder.extractVehicleMakeObservation(results),
                        vehicleType: self.audioRecorder.extractVehicleTypeObservation(results)
                    )
            }
        }
    }
    
    let recordMessage: String
    let foregroundColor = Color.white
    let backgroundColor = Color.green
    let rerecordColor = Color.orange
    let fileURL: URL? = nil
}


struct AudioRecorderView: View {
    @EnvironmentObject var store: ObservationStore
    @StateObject var audioRecorder = AudioRecorderImpl()

    var body: some View {
        VStack {
            RecordButton(
                audioRecorder: audioRecorder,
                recordMessage: "Please say: vehicle license plate, vehicle make, and vehicle type. Go ahead."
            )
            if store.mostRecentObservation.isInitialized {
                HStack {
                    PlayButton(audioRecorder: audioRecorder)
                    ClearButton(audioRecorder: audioRecorder)
                }.padding(.top, 5)
                    
            }
           // Text("\(self.audioRecorder.transcript ?? "")")
        }.padding(.horizontal, 22)
        .padding(.vertical, 10)
        .background(Color(UIColor.systemGray3))
    }
    
    let fileURL: URL? = nil
}

struct AudioRecorderButton_Previews: PreviewProvider {
    static var previews: some View {
        AudioRecorderView()
    }
}
