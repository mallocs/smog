//
//  ContentView.swift
//  SmogSpotter
//
//  Created by mallocs.net on 9/22/20.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var store: ObservationStore
    var body: some View {
        VStack {
            AudioRecorderView()
                .background(Color(UIColor.systemGray3))
                .padding(.top, 8)
            ObservationView(observation: store.mostRecentObservation)
        }.background(Color(UIColor.systemGray3))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



//TabView {
//    SpotterView()
//        .tabItem {
//            Image(systemName: "photo")
//            Text("Spot a vehicle")
//        }
//    ObservationView()
//        .tabItem {
//            Image(systemName: "square.and.pencil")
//            Text("Submit observation")
//        }
//}
//.font(.headline)
