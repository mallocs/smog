//
//  SmogSpotterApp.swift
//  SmogSpotter
//
//  Created by mallocs.net on 9/22/20.
//

import SwiftUI

@main
struct SmogSpotterApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @StateObject var store = ObservationStore()

    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(store)
        }
    }
}
