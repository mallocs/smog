//
//  Observation.swift
//  SmogSpotter
//
//  Created by mallocs.net on 11/19/20.
//

import Foundation
import CoreLocation

class ObservationObject: ObservableObject, Identifiable
{
    var lm = LocationManager()
    @Published var isInitialized = false
    @Published var model = Observation()
    @Published var isValid = false
    
    var id: UUID {
        get {
            model.id
        }
    }
    var licensePlateNumber: String {
        get {
            model.licensePlateNumber ?? ""
        }
        set(newLicensePlateNumber) {
            model.licensePlateNumber = newLicensePlateNumber
            isValid = validate()
        }
    }
    var vehicleMake: VehicleMake? {
        get {
            model.vehicleMake
        }
        set(newVehicleMake) {
            model.vehicleMake = newVehicleMake
            isValid = validate()
        }
    }
    var vehicleType: VehicleType? {
        get {
            model.vehicleType
        }
        set(newVehicleType) {
            model.vehicleType = newVehicleType
            isValid = validate()
        }
    }
    var observationTime: Date {
        get {
            model.timestamp
        }
        set(newObservationTime) {
            model.timestamp = newObservationTime
        }
    }
    var street: String {
        get {
            model.street ?? ""
        }
        set(newStreet) {
            model.street = newStreet
            isValid = validate()
        }
    }
    var city: String {
        get {
            model.city ?? ""
        }
        set(newCity) {
            model.city = newCity
            isValid = validate()
        }
    }
    var county: County? {
        get {
            model.county
        }
        set(newCounty) {
            model.county = newCounty
            isValid = validate()
        }
    }
    
    func reset() {
        model = Observation()
        isInitialized = false
    }
    
    // The assumption is these values should be initialized when it is recorded, ie where/when the observation happens.
    // An observation can be created when the app is first opened. It can be modified after it's recorded, but
    // values set here should only be touched when it's first recorded.
    func initObservation() {
        var latitude: CLLocationDegrees  { lm.location?.latitude ?? 0 }
        var longitude: CLLocationDegrees { lm.location?.longitude ?? 0 }
        var placemark: String { return("\(lm.placemark?.description ?? "Unknown location")") }
        model.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        model.placemark = lm.placemark
        model.timestamp = Date()
        isInitialized = true
        geocodeObservation()
    }

    func dateFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: observationTime)
    }
    
    func timeFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mma"
        return dateFormatter.string(from: observationTime)
    }
    
    func urlRequest(url: URL?) -> URLRequest {
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.encodeParameters(parameters: ["license": licensePlateNumber,
                                              "type": "\(vehicleMake!),\(vehicleType!)",
                                              "obdate": dateFormat(),
                                              "obtime": timeFormat(),
                                              "street": street,
                                              "city": city])
        return request
    }

    func validate() -> Bool {
        if licensePlateNumber == ""
            || vehicleMake == nil
            || vehicleType == nil
            || street == ""
            || city == ""
            || county == nil {
            return false
        }
        return true
    }

    func submit(_ handleSubmitCompletion: SubmitCompletionHandler? = nil) {
        guard validate() else {
            if let handleSubmitCompletion = handleSubmitCompletion {
                handleSubmitCompletion(nil)
            }
            return
        }
        let url = URL(string: "http://127.0.0.1:3000/post")
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
          "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest(url: url)) { (data, response, error) in
                // Check for Error
                if let error = error {
                    let errString = "Error took place \(error)"
                    if let handleSubmitCompletion = handleSubmitCompletion {
                        handleSubmitCompletion(errString)
                    }
                    return
                }
                // Convert HTTP Response Data to a String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    let successString = "Response data string:\n \(dataString)"
                    if let handleSubmitCompletion = handleSubmitCompletion {
                        handleSubmitCompletion(successString)
                    }
                    self.reset()
                }
        }
        task.resume()
    }

    func geocodeObservation() {
        guard let coordinate = self.model.coordinate, self.model.placemark == nil else {
            return
        }
        CLGeocoder().reverseGeocodeLocation(
            CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude), completionHandler: { (places, error) in
                if error == nil {
                    self.model.placemark = places?[0]
                    if let streetString = self.model.placemark?.name, self.street == "" {
                        self.street = streetString
                    }
                    if let placemarkCity = self.model.placemark?.locality, self.city == "" {
                        self.city = placemarkCity
                    }
                    if let countyString = self.model.placemark?.subAdministrativeArea, self.county == nil {
                        self.county = County(rawValue: countyString.lowercased())
                    }
                }
        })
    }
    
    func updateObservation(timestamp: Date? = nil, licensePlateNumber: String? = nil, vehicleMake: VehicleMake? = nil, vehicleType: VehicleType? = nil) {
        if let timestamp = timestamp {
            self.observationTime = timestamp
        }
        if let licensePlateNumber = licensePlateNumber {
            self.licensePlateNumber = licensePlateNumber
        }
        if let vehicleType = vehicleType {
            self.vehicleType = vehicleType
        }
        if let vehicleMake = vehicleMake {
            self.vehicleMake = vehicleMake
        }
    }
}

// https://gist.github.com/HomerJSimpson/80c95f0424b8e9718a40#gistcomment-2385795
extension URLRequest {
  private func percentEscapeString(_ string: String) -> String {
    var characterSet = CharacterSet.alphanumerics
    characterSet.insert(charactersIn: "-._* ")
    
    return string
      .addingPercentEncoding(withAllowedCharacters: characterSet)!
      .replacingOccurrences(of: " ", with: "+")
      .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
  }
  
  mutating func encodeParameters(parameters: [String : String]) {
    httpMethod = "POST"
    
    let parameterArray = parameters.map { (arg) -> String in
      let (key, value) = arg
      return "\(key)=\(self.percentEscapeString(value))"
    }
    
    httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
  }
}


