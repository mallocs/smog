//
//  ObservationStore.swift
//  SmogSpotter
//
//  Created by mallocs.net on 9/23/20.
//

import UIKit
import CoreML
import Vision
import CoreLocation
import AVFoundation
import MapKit

class ObservationStore: ObservableObject
{
    // make sure we always have at least one ObservationObject
    @Published private (set) var observations = [ObservationObject()]
    var lm = LocationManager()
    
    func addObservation(_ observation: ObservationObject) {
        observations.append(observation)
    }
    
    var mostRecentObservation: ObservationObject {
        get {
            return observations.last!
        }
    }
    
    @discardableResult
    func removeObservation(_ observation: ObservationObject) -> Bool {
        if let index = observations.firstIndex(matching: observation) {
            observations.remove(at: index)
            return true
        }
        return false
    }
    
    func removeMostRecentObservation() {
        self.observations.remove(at: self.observations.count - 1)
    }
    
    func updateObservation(_ observation: ObservationObject) {
        if let index = observations.firstIndex(matching: observation) {
            observations[index] = observation
        }
    }
    
    func updateObservation(_ observationId: UUID,
                           timestamp: Date? = nil,
                           licensePlateNumber: String? = nil,
                           vehicleMake: VehicleMake? = nil,
                           vehicleType: VehicleType? = nil) {
        if let index = observations.firstIndex(matchingId: observationId) {
            observations[index].updateObservation(timestamp: timestamp,
                                                  licensePlateNumber: licensePlateNumber,
                                                  vehicleMake: vehicleMake,
                                                  vehicleType: vehicleType)
        }
    }
}

extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
