//
//  ObservationView.swift
//  SmogSpotter
//
//  Created by mallocs.net on 9/28/20.
//

import SwiftUI
import CoreLocation

struct ObservationView: View {

    @ObservedObject var lm = LocationManager()
    @StateObject var observation: ObservationObject
    @State var tryingToSubmit = false

    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("Vehicle Description")) {
                        HStack {
                            if tryingToSubmit && observation.licensePlateNumber == "" {
                                Text("*").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.vertical, -25)
                            }
                            Text("License plate:")
                            TextField("Enter number", text: $observation.licensePlateNumber)
                        }
                        HStack{
                            if tryingToSubmit && observation.vehicleMake == nil {
                                Text("*").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.trailing, -7)
                            }
                            Picker(
                                selection: $observation.vehicleMake,
                                label: Text("Vehicle Make:")
                            ) {
                                Text("").tag(nil as VehicleMake?)
                                ForEach(VehicleMake.allCases) { vehicleMake in
                                    Text(vehicleMake.rawValue).tag(vehicleMake as VehicleMake?)
                                }
                            }
                        }
                        HStack{
                            if tryingToSubmit && observation.vehicleType == nil {
                                Text("*").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.trailing, -7)
                            }
                            Picker(
                                selection: $observation.vehicleType,
                                label: Text("Vehicle Type:")
                            ) {
                                Text("").tag(nil as VehicleType?)
                                ForEach(VehicleType.allCases) { vehicleType in
                                    Text(vehicleType.rawValue).tag(vehicleType as VehicleType?)
                                }
                            }
                        }
                    }

                    Section(header: Text("Observation Time")) {
                        VStack {
                            HStack {
                                Spacer()
                                DatePicker("Enter observation date and time: ", selection: $observation.observationTime).labelsHidden()
                                Spacer()
                            }
                        }
                    }

                    Section(header: Text("Observation Location")) {
                        VStack {
                            HStack {
                                if tryingToSubmit && observation.street == "" {
                                    Text("*").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.vertical, -25)
                                }
                                Text("Street and/or major cross street: ")
                                Spacer()
                            }
                            TextField("Enter street", text: $observation.street)
                        }
                        HStack(spacing: 0) {
                            if tryingToSubmit && observation.city == "" {
                                Text("*").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.vertical, -25)
                            }
                            Text("City:")
                            Spacer()
                            TextField("Enter city or nearest city ", text: $observation.city)
                            Spacer()
                        }
                        HStack{
                            if tryingToSubmit && observation.county == nil {
                                Text("*").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.trailing, -7)
                            }
                            Picker("Select county: ", selection: $observation.county) {
                                
                               // Text("Required").foregroundColor(Color.red).tag(nil as County?)
                                Text("").tag(nil as County?)
                                ForEach(County.allCases) { county in
                                    Text(county.print).tag(county as County?)
                                }
                            }
                        }
                    }
                    
                    Section {
                        VStack(spacing: 0) {
                            Spacer()
                            HStack {
                                Spacer()
                                if tryingToSubmit && !observation.isValid {
                                    Text("*").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.trailing, -7)
                                    Text("Missing required field").foregroundColor(Color.red).font(.callout)
                                } else {
                                    Text(" ").foregroundColor(Color.red).font(.largeTitle).baselineOffset(-12).padding(.trailing, -7)
                                    Text(" ").foregroundColor(Color.red).font(.callout)
                                }
                                Spacer()
                            }
                            HStack {
                                Spacer()
                                SubmitButton(disable: tryingToSubmit && !observation.isValid, submitCompletionHandler: { result in
                                    if result == nil {
                                        tryingToSubmit = true
                                    }
                                }).padding(.horizontal, 22)
                                Spacer()
                            }
                        }.padding(.top, -25)
                    }
                    .listRowInsets(EdgeInsets())
                    .background(Color(UIColor.systemGray6))
                    .textCase(nil)
                }
            }.navigationBarHidden(true)
//            .simultaneousGesture(
//                TapGesture()
//                    .onEnded { _ in
//                        UIApplication.shared.endEditing()
//                    }
//            )
        }
    }
}

struct ObservationView_Previews: PreviewProvider {
    static var previews: some View {
        ObservationView(observation: ObservationObject())
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}


// This section just for debugging for now
//    var latitude: String  { return("\(self.observation.model.coordinate?.latitude ?? 0)") }
//    var longitude: String { return("\(self.observation.model.coordinate?.longitude ?? 0)") }
//    var placemark: String { return("\(self.observation.model.placemark?.name ?? "Unknown location")") }
//  var status: String    { return("\(lm.status)") }


//                    Section {
//                        Text("Latitude: \(self.latitude)")
//                        Text("Longitude: \(self.longitude)")
//                        Text("Placemark: \(self.placemark)")
//                    }


// .environmentObject(ObservationStore())
